<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Models\Product;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app ->add(new logging());//הוספה של טבעת מסביב  לראווט. כל פעם שאנחנו עושים עוד שאנחנןו מוסיפים טבעת נוספת 

$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
$app->get('/customers/{number}', function($request, $response,$args){
   return $response->write('your customer number is '.$args['number']);
});
$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('customer '.$args['cnumber'].' purchased product number '.$args['pnumber'] );
});


$app->delete('/products/{product_id}', function($request, $response,$args){
    $user = Product::find($args['product_id']); //find the user in database
    $user->delete(); //delete the user
    if($user->exists){ //check if the user already exit
        return $response->withStatus(400); //exist -> false error
    } else {
        return $response->withStatus(200); //user delete succesfully
    }
});


$app->get('/products', function($request, $response,$args){
   $_product = new Product();
   $products = $_product->all();
   $payload = [];
   foreach($products as $prod){
        $payload[$prod->id] = [
            'id'=> $prod->id,
            'name'=> $prod->name,
            'price'=> $prod->price];

    }
   return $response->withStatus(200)->withJson($payload);
});

$app->post('/products', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name',''); // משיכת השדה לגוף ההודעה
    $price = $request->getParsedBodyParam('price',''); // משיכת השדה לגוף ההודעה
    $_product = new Product();
    $_product->name =  $name; // גוף ההודעה שווה למה שהגיע בפוסט
    $_product->price =$price;
    $_product->save();
    if($_product->id){ // אם יש להודעה איידי זה אומר שההודעה נשמרה כמו שצריך בדיבי
        $payload = ['product_id'=>$_product->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }

});

$app->put('/products/{product_id}', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name',''); //get the parameters from the post
    $price = $request->getParsedBodyParam('price',''); //get the parameters from the post
    $_product = Product::find($args['product_id']); // search the user with the id from the url
    $_product->name = $name; //delete the old param, and insert the new param
    $_product->price = $price;
    if($_product->save()){ //the update succeed
        $payload = ['product_id'=>$_product->id, "result"=>"The product has been update succesfully"]; //useful to the json that return to the user about the update status
        return $response->withStatus(201)->withJson($payload); //succes
    } else{
        return $response->withStatus(400); //error
    }
});
$app->get('/products/{id}', function($request, $response, $args){
    $name = Product::find($args['id']);
    return $response->withStatus(200)->withJson(json_decode($name));
});






































//-------------------------------MESSAGES---------------------------------------------------------------------------------------
$app->get('/messages', function($request, $response,$args){
    $_message = new Message();
    $messages = $_message->all();

    $payload = [];
    foreach($messages as $msg){
        $payload[$msg->id] = [
            "body" => $msg->body,
            "user_id" => $msg->user_id,
            "created_at" => $msg->created_at
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message',''); // משיכת השדה לגוף ההודעה
    $userid = $request->getParsedBodyParam('userid',''); // משיכת השדה לגוף ההודעה
    $_message = new Message();
    $_message->body =  $message; // גוף ההודעה שווה למה שהגיע בפוסט
    $_message->user_id =$userid;
    $_message->save();
    if($_message->id){ // אם יש להודעה איידי זה אומר שההודעה נשמרה כמו שצריך בדיבי
        $payload = ['message_id'=>$_message->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }

});

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $_message = Message::find($args['message_id']); // מציאת ההודעה הרלוונטית לפי האיידי
    $_message->delete();
    if($_message->exist){
        return $response->withStatus(400);
    }
    else{
        return $response->withStatus(200);
        
    }
});

//lesson 4 19/11/17
$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message',''); // משיכת השדה לגוף ההודעה שהגיע מהפוסט
    $_message = Message::find($args['message_id']);//מחפש את ההודעה עם האיי די שהגיע מהיו אר אל
    //die("message id = ". $_message->id);//אפשרות לבדור את עצמי. הקוד יעבוד ע כאן
    $_message ->body =$message;// השורה שבה קורה העדכון של הכנסת ערך חדש להודעה
    if( $_message->save()){ // אם יש להודעה איידי זה אומר שההודעה נשמרה כמו שצריך בדיבי
        $payload = ['message_id'=>$_message->id,"result"=>"The message was updated successfuly"];//החזרה של קובץ גייסון כדי שהיוזר ידע אם התבצע חיבור (200 או 201 או 400)
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }

});

$app->post('/messages/bulk', function($request, $response,$args){
    $payload =$request->getParsedBody();//ה"פארס" הופך את מה שקיבלנו כגייסון  למערך
    Message:: insert($payload);
    return $response->withStatus(201)->withJson($payload);
    }

);




//------------------------USERS---------------------------------------------------------------------------------
$app->get('/users', function($request, $response,$args){
   $_user = new User();
   $users = $_user->all();
   $payload = [];
   foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=> $usr->id,
            'username'=> $usr->username,
            'email'=> $usr->email];

    }
   return $response->withStatus(200)->withJson($payload);
});

//------------------------------------------------------------------------------------------------------------------



$app->get('/users/{id}', function($request, $response,$args){
$_id = $args['id'];
$user = User::find($_id);

   return $response->withStatus(200)->withJson($user);
});

//------------------------------------------------------------------------------------------------------------------
$app->post('/users', function($request, $response,$args){
    $username = $request->getParsedBodyParam('username','');
    $email = $request->getParsedBodyParam('email','');
   $_user = new User();
   $_user->username = $username;
   $_user->email = $email;
   $_user->save();
   if($_user->id){
       $payload = ['user id: '=>$_user->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});

//----------------------------------------------------------------------------------------------------------------------
$app->delete('/users/{id}', function($request, $response,$args){
    $user = User::find($args['id']); 
    $user->delete();
    if($user->exist){
        return $response->withStatus(400);
    }
    else{
        return $response->withStatus(200);
        
    }
});
//-----------------------------------------------------------------------------------------------------------------------
$app->post('/users/bulk', function($request, $response,$args){
    $payload =$request->getParsedBody();//ה"פארס" הופך את מה שקיבלנו כגייסון  למערך
    User:: insert($payload);
    return $response->withStatus(201)->withJson($payload);
    }

);

//PUT USER - update user from database
$app->put('/users/{user_id}', function($request, $response,$args){
    $user = $request->getParsedBodyParam('username',''); //get the parameters from the post
    $email = $request->getParsedBodyParam('email',''); //get the parameters from the post
    $_user = User::find($args['user_id']); // search the user with the id from the url
    $_user->username = $user; //delete the old param, and insert the new param
    $_user->email = $email; //delete the old param, and insert the new param
    if($_user->save()){ //the update succeed
        $payload = ['user_id'=>$_user->id, "result"=>"The user has been update succesfully"]; //useful to the json that return to the user about the update status
        return $response->withStatus(201)->withJson($payload); //succes
    } else{
        return $response->withStatus(400); //error
    }
});

//-------------------------------------------------------------------------------------------------------------------------
//$app->delete('/users/bulk', function($request, $response,$args){
  //  $payload =$request->getParsedBody();//ה"פארס" הופך את מה שקיבלנו כגייסון  למערך
//User::trash($payload);
  //  return $response->withStatus(201)->withJson($payload);
  //  }


//);
//-----------------------------------3/12/17------------------------------------------------------
$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});




$app->get('/messages/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $message = Message::find($_id);
    return $response->withStatus(200)->withJson($message);
});

//Login without JWT
$app->post('/login', function($request, $response,$args){
    $username  = $request->getParsedBodyParam('username','');
    $password = $request->getParsedBodyParam('password','');    
    $_user = User::where('username', '=', $username)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
});

/*$app->post('/auth', function($request, $response,$args){//צריך לקבל שם משתשמש וסיסמא, לבדוק זכאות, לשלוח לקליינט   גייסון וב טוקן מתאים 
$user=$request->getParsedBodyParam('user','');//קיבלנו את היוזרניים מהקליינט
$password=$request->getParsedBodyParam('password','');
//we need to do the DB but not now
if($user == 'jack' && $password =='1234'){//בדיקת זכאות
    //create jwt and send to client, we need to generate jwt but not now
    $payload = ['token'=>'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3IiwibmFtZSI6ImphY2siLCJhZG1pbiI6dHJ1ZX0.PETyPBssJTN1V8fSwPxkE0Hftg3hGYux4Rx1WcMC5NA'];
    return $response->withStatus(200)->withJson($payload);
}else{
     $payload = ['token'=>null];//אם אין לו טוקן
    return $response->withStatus(403)->withJson($payload);
     }

   
});

$app->add(new \Slim\Middleware\JwtAuthentication([//הגנה על כל הראוטים- רק מי שיש לו גייסון ובב טוקן יקבל את הראוטים השונים
    "secret" => "supersecret",//מידל וור שבודק שכל גייסון טוקן שמגיע- האם הוא מאומתאו יותר נכון אמיתי
    "path" =>['/messages']//אלה הראוטרים שאנחנו רוצים שגיי דאבליו טי יפקח עליהם
]));*/


$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);//תוסיף להאדר את האישור לדומיינים אחרים גם
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();